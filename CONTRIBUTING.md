# How to contribute

## Testing

### Static tests
```shell
poetry install --with test
flake8 ./src
```

### Unit test
```shell
poetry install
python -m unittest discover tests.unit 
```

### Integration
```shell
# Deactivate any python virtual environment
deactivate
cd tests/integration/demo
python3 -m venv venv && source venv/bin/activate
poetry install
# Test the CLI runs successfully
netbox-nms
```
