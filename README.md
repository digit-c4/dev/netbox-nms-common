
## Usage
```bash
poetry add git+https://code.europa.eu/digit-c4/dev/netbox-nms-common.git#0.1.0
```

### Manage shortcut
Sick of manually entering the path to your Netbox `manage.py` script? use this shortcut instead
```bash
netbox-nms MY-COMMAND
# or, for usage in CI pipeline
python -m netbox_nms_common.manage MY-COMMAND
```

This command will look for the `manage.py` script and execute the first available in
* `${NETBOX_ROOT}/netbox/manage.py` if `NETBOX_ROOT` env variable is available
* `./lib/netbox-core/netbox/manage.py`
* `./lib/netbox/netbox/manage.py`
* `/opt/netbox/netbox/manage.py`

### Auto plugin configuration
Are you also tired with the maintenance of your Netbox plugin attributes? Use the ``
```python
from netbox_nms_common.plugin_config import AutoPluginConfig


class NetboxMyConfig(AutoPluginConfig):
    """Netbox Plugin Configuration class"""
    
    # No more version, description, author or author_email to manage
    # Taken from package metadata !
    name = __package__
    verbose_name = "Netbox Plugin"
    base_url = "my"


config = NetboxMyConfig
```

## Test
```bash
poetry isntall --only test
```

### Static
```bash
autopep8 --in-place -r src
```
