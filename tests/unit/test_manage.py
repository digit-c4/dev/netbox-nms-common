import logging
import unittest
import os
import io
import contextlib
from unittest import mock

from netbox_nms_common.manage import dotenv_config_autoset


class TestDotenvConfigAutoset(unittest.TestCase):

    def test_info_logging_netbox_configuration_set(self):
        netbox_configuration = "any.blabetiblou.class"
        f = io.StringIO()
        logging.getLogger().setLevel(logging.INFO)
        with contextlib.redirect_stderr(f):
            with mock.patch.dict(os.environ, {"NETBOX_CONFIGURATION": netbox_configuration}):
                dotenv_config_autoset()
                self.assertEqual(os.getenv("NETBOX_CONFIGURATION"), netbox_configuration)

        self.assertTrue(f"'NETBOX_CONFIGURATION' is set to '{netbox_configuration}' and won't be overwritten" in f.getvalue())

    def test_no_netbox_configuration_no_dotenv(self):
        names_to_remove = {"NETBOX_CONFIGURATION"}
        modified_environ = {
            k: v for k, v in os.environ.items() if k not in names_to_remove
        }
        with mock.patch.dict(os.environ, modified_environ, clear=True):
            dotenv_config_autoset()
            self.assertEqual(os.getenv("NETBOX_CONFIGURATION"), None)

    @mock.patch("os.path.isfile")
    def test_no_netbox_configuration_with_dotenv(self, isfile_mock):
        dotenv_path = os.path.join(os.getcwd(), '.env')
        isfile_mock.side_effect = lambda path: True if path == dotenv_path else False

        names_to_remove = {"NETBOX_CONFIGURATION"}
        modified_environ = {
            k: v for k, v in os.environ.items() if k not in names_to_remove
        }
        with mock.patch.dict(os.environ, modified_environ, clear=True):
            dotenv_config_autoset()
            self.assertEqual("netbox_nms_common.dotenv_config", os.getenv("NETBOX_CONFIGURATION"))


if __name__ == '__main__':
    unittest.main()
