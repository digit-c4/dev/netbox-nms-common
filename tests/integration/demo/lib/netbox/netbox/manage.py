
main_executed = False

if __name__ == "__main__":
    main_executed = True

if not main_executed:
    raise Exception(f"__name__ = '{__name__}' != '__main__' => main not executed")

print("manage.py main successfully executed")
