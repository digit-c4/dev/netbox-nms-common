import logging
import os
import sys
import subprocess


def find_manage_py_script():
    search = [
        os.path.join(os.getcwd(), 'lib', 'netbox', 'netbox', 'manage.py'),
        os.path.join(os.getcwd(), 'lib', 'netbox-core', 'netbox', 'manage.py'),
        os.path.join('/opt', 'netbox', 'netbox', 'manage.py'),
    ]

    netbox_root = os.getenv('NETBOX_ROOT')
    if netbox_root is not None:
        s = os.path.join(netbox_root, 'netbox', 'manage.py')
        logging.debug(
            f"Got NETBOX_ROOT env = `{netbox_root}` and will therefore start searching for manage script in `{s}`")

        search = [s, *search]

    for s in search:
        if os.path.isfile(s):
            return s, search

    return None, search


def dotenv_config_autoset():

    if 'NETBOX_CONFIGURATION' in os.environ:
        logging.info(
            f"'NETBOX_CONFIGURATION' is set to '{os.getenv('NETBOX_CONFIGURATION')}' and won't be overwritten")
        return

    dotenv_path = os.path.join(os.getcwd(), '.env')
    if os.path.isfile(dotenv_path):
        logging.warning("'NETBOX_CONFIGURATION' environment wasn't set while a .env file was found in the current "
                        "directory. the environment variable will therefore be set to configure netbox so that it "
                        "loads the configuration from there. If you really want to use the default configuration "
                        "style, you need to set the 'NETBOX_CONFIGURATION' variable to 'netbox.configuration' ("
                        "`export NETBOX_CONFIGURATION=netbox.configuration`)")

        os.environ['NETBOX_CONFIGURATION'] = "netbox_nms_common.dotenv_config"


def manage():
    logging.debug(sys.argv)

    dotenv_config_autoset()

    (manage_script_path, search) = find_manage_py_script()

    if manage_script_path is None:
        raise Exception(
            f"Couldn't find the manage.py script in any of {search}")

    subprocess.run(['python', manage_script_path, *sys.argv[1:]])


if __name__ == "__main__":
    manage()
