import json
import logging
import os

from dotenv import dotenv_values


config = {}
input_config = dotenv_values(".env")

NETBOX_ENV_PREFIX = os.getenv("NETBOX_ENV_PREFIX", "")


# TODO add NETBOX_CONFIGURATION_DEFAULT

def merge_config(config, conf_key, parent_keys, value, nested_conf_keys):
    for nested_conf_key, upper_nested_conf_keys in nested_conf_keys.items():
        if conf_key.startswith(f"{nested_conf_key}_"):
            nested_config = config.setdefault(nested_conf_key, {})
            merge_config(nested_config, conf_key[len(
                nested_conf_key)+1:], [*parent_keys, nested_conf_key], value, upper_nested_conf_keys)
            return

    json_parsed = False
    try:
        value = json.loads(value)
        json_parsed = True
    except:  # noqa: E722
        pass

    logging.debug(
        f"Loaded `{'.'.join([*parent_keys,conf_key])}` config from "
        f"`{NETBOX_ENV_PREFIX}{'_'.join([*parent_keys,conf_key])}` env "
        f"key{' (json encoded value)' if json_parsed else ' (NON json encoded value)'}: {value}")

    config[conf_key] = value


for key, value in input_config.items():
    if key.startswith(NETBOX_ENV_PREFIX):
        conf_key = key[len(NETBOX_ENV_PREFIX):]

        nested_conf_keys = {'DATABASE': {},
                            'REDIS': {'tasks': {}, 'caching': {}}}
        merge_config(config, conf_key, [], value, nested_conf_keys)

for key, value in config.items():
    globals()[key] = value
