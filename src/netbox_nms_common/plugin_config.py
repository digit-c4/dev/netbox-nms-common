import importlib.metadata


# Info: Breaking-change, will not be compatible with Netbox core < 4.2.x
from netbox.plugins import PluginConfig  # type: ignore pylint: disable=E0401


class AutoPluginConfig(PluginConfig):
    """Netbox Plugin Configuration class"""

    def __init__(self, *args, **kwargs):
        assert self.__class__.name

        self.__class__.version = importlib.metadata.version(
            self.__class__.name)
        metadata = importlib.metadata.metadata(self.__class__.name)
        self.__class__.description = metadata.get('summary')
        maintainer = metadata.json.get('maintainer')
        self.__class__.author = maintainer or metadata.json.get('author')
        self.__class__.author_email = metadata.json.get('maintainer_email' if maintainer else 'author_email')

        super().__init__(*args, **kwargs)
